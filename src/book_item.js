import React from 'react';
import { Router, Route, Link } from "react-router-dom";
import axios from 'axios';
import convert from 'xml-js';
import  parseJson from 'parse-json';
import './index.css';
import { Card } from 'react-bootstrap';


  
  class book_item extends React.Component {
    constructor(props){
      super(props);
      this.state = {query: '',books: []};
      this.onChange = e => {
        const { value } = e.target;
        this.setState({
          query: value
        });
    
        this.search(value);
      };

      this.search = query => {
        axios.get(`https://www.goodreads.com/search.xml?key=fbtAHIQHuoLjyTddE1Mpw&q=${query}`)
          .then(data => {
            if (query != '') {
              const books = parseJson(convert.xml2json(data.data)).elements[0].elements[1].elements[6].elements;
              console.log(books);
              this.setState({ books }, () => {});
            }
          });
        };
    }
    componentDidMount(){
      this.search("");
    }
    
    render () {
     if(this.state.books == [] || this.state.search == "a" || this.state.search == "A" || this.state.books == undefined){
        return(
          <div class="container search-books">
              <h1>Hello Monsieur T-shirt ! </h1>
              <input class="form-text" placeholder="Hunger Games" type="text" name="query" onChange={this.onChange.bind(this)}/>
              <p>Aucun Livre trouvé</p>
          </div>
        )
      }
      else{
          return (
            <div class="container search-books">
              <h1>Hello Monsieur T-shirt ! </h1>
              <input class="form-text" placeholder="Hunger Games" type="text" name="query" onChange={this.onChange.bind(this)}/>
              <div className="search-div">
                {this.state.books.map( book =>
                <Link to={`/book/${book.elements[8].elements[0].elements[0].text}`} key={book.elements[8].elements[0].elements[0].text}> 
                  <Card className="book">
                    <Card.Img src = {book.elements[8].elements[4].elements[0].text}/> 
                    <Card.Title>{book.elements[8].elements[1].elements[0].text}</Card.Title>
                  </Card>
                </Link>
                )}
              </div>
          <p> Pour utiliser l'api , il faut installé l'extention <a href="https://chrome.google.com/webstore/detail/moesif-orign-cors-changer/digfbfaphojjndkpccljibejjbppifbc"> "Moesif Orign & CORS Changer" </a> sur google chrome </p> 
        </div>
        
          )
        }
      }
    }

export default book_item;