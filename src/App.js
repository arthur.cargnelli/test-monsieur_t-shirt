import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Book from './book_item';
import BookDescription from './bookdescription';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";



class App extends Component {
  render() {
    return (
      <Router>
      <Switch>
        
        
        <Route name="BookDescription" path="/book/:id" component={BookDescription} />
        <Route path="/" component={Book} />
        
      </Switch>
      </Router>
    );
  }
}



export default App;
