import React from 'react';
import './index.css';
import axios from 'axios';
import convert from 'xml-js';
import  parseJson from 'parse-json';
import { Link } from "react-router-dom";


class BookDescritption extends React.Component{
    constructor(props){
        super(props);
        this.state= {book:null, loading: true}
        this.logFields = s => {
            const { book } = this.state;
            console.log(book)
        }
        axios.get(`https://www.goodreads.com/book/show/${props.match.params.id}.xml?key=fbtAHIQHuoLjyTddE1Mpw`)
            .then(data => {
                this.setState({
                    book: parseJson(convert.xml2json(data.data)).elements[0].elements[1],
                    loading: false
                }, this.logFields);
            });
        };
    render(){
        if(this.state.loading){
            return <div container ><p> Chargnement ...</p></div>
        }
        else{
            return(
            <div class="container book-description">
                <div class="title-img">
                    <h1> {this.state.book.elements[1].elements[0].text || this.state.book.elements[1].elements[0].cdata } </h1>
                </div>
                <div className="description">
                    <img src={this.state.book.elements[8].elements[0].text || this.state.book.elements[8].elements[0].cdata } alt={this.state.book.elements[1].elements[0].text || this.state.book.elements[1].elements[0].cdata } />
                   <div> <p>{this.state.book.elements[16].elements[0].cdata || this.state.book.elements[16].elements[0].text}</p> </div>
                </div>
                <div class ="links">
                    <Link to="/" > Retour </Link>
                </div>
            </div>)
        }
    }
}

export default BookDescritption;
